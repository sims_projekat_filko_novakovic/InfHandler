from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.structure_dock_widget import StructureDockWidget

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/ikonica.png"))
        self.resize(1000,650)

        self.tool_bar = ToolBar(parent=self)
        
        self.menu_bar = MenuBar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
<<<<<<< HEAD
        self.workspace_widget = StructureDockWidget(title="Workspaces",parent=self)
       # self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)
=======
        self.tool_bar.createAppDemo(self.central_widget.tab1)
       #self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)
>>>>>>> bfcf117cdba172907a2e32dd01252d2e5172c4ab
        #self.file_dock_widget2 = StructureDockWidget("Baze", self)

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.workspace_widget, Qt.Vertical)
#        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)
        #Kasnije ovo je moguce izmeniti u neku bazu ili slicno
  #      self.addDockWidget(Qt.RightDockWidgetArea, self.file_dock_widget2, Qt.Vertical)

        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Milos Novakovic"))

    def get_actions(self):
        return self.tool_bar.actions_dict
        