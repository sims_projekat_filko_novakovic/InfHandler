from PySide2.QtWidgets import QWidget, QVBoxLayout
from gui.widgets.table_view.test_table_view import TestTableView
from gui.widgets.table_view.data_view import AppDemo

class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.cw_Layout = QVBoxLayout(self)
        #self.table = TestTableView()
        #self.table1 = TestTableView()
        
        self.tab1 = AppDemo(self)

        self.cw_Layout.addWidget(self.tab1)
        #self.cw_Layout.addWidget(self.table1)

        self.setLayout(self.cw_Layout)

    