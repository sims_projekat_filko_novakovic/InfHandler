import sys
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit, QTabWidget, QGridLayout, QVBoxLayout, QTableView
from .test_table_view import TestTableView
from gui.widgets.table_view.table_editor import EditDialog
from .complex_tab import Tab

class AppDemo(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.resize(800, 600)
        self.tabCollection = []

        mainLayout = QGridLayout()
        self.tabs1 = QTabWidget(tabsClosable=True)      

        self.tabs1.setTabsClosable(True)

        self.tabs1.tabCloseRequested.connect(self.on_close_tab)

        mainLayout.addWidget(self.tabs1, 0, 0)
        mainLayout.addWidget(self.tabs1,0 ,0)
        self.setLayout(mainLayout)


    def create_tab(self, description, data, linked_inbound,linked_outbound):
        self.header_data = description
        inbound_tables = []
        outbound_tables = []
        for element in linked_inbound:
            inbound_tables.append({"header":element,"data":element.get_data()})
        for element in linked_outbound:
            outbound_tables.append({"header":element,"data":element.get_data()})
        TestTable = Tab(parent=self,header_data=description,table_data=data,inbound_tabs=inbound_tables,outbound_tabs=outbound_tables)
        self.tabCollection.append(TestTable)
        self.tabs1.addTab(TestTable, description.title)


    def on_close_tab(self, index):
        self.tabs1.removeTab(index)
        self.tabCollection.pop(index)
        
    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dialoga podacima iz reda tabele
        if self.parent().parent().parent().data_source_type == "file":
            edit_dialog.enter_data(selected_labels, selected_data)

        # elif self.parent().parent().parent().data_source_type == "database":
        #     primary_keys = []
        #     connection_data = self.parent().parent().parent().connection_data
        #     connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
        #     data = execute_statement(connection, get_primary_keys(connection_data["table"], connection_data["db_name"]))
        #     for pk in data:
        #         primary_keys.append(pk["column_name"])
        #     not_nullable = []
        #     connection_data = self.parent().parent().parent().connection_data
        #     connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
        #     data = execute_statement(connection, get_not_null_columns(connection_data["table"], connection_data["db_name"]))
        #     for nn in data:
        #         not_nullable.append(nn["COLUMN_NAME"])
        #     edit_dialog.enter_data(selected_labels, selected_data, read_only=primary_keys, not_null=not_nullable)
        # prikazivanje dijaloga

        result = edit_dialog.exec_()
        if result == 1: # izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)


            # # TODO: dodati da se prosledi SQL upit ukoliko je tabela iz baze
            # if self.parent().parent().parent().data_source_type == "database":
            #     connection_data = self.parent().parent().parent().connection_data
            #     connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
    

class Tab():
    def __init__(self, table=None):
        self.table = table
        # self.description = description
        # self.inbound_description = inbound_description
        # self.outbound_description = outbound_description

    def generate_tab(self, table):
        Table = TestTableView(data=self.table)
        return Table