# import sys
# from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QStyledItemDelegate, QLineEdit, QListWidget, QAbstractItemView
# from gui.widgets.table_view import test_table_model, test_table_view

# class DelegateLEdit(QStyledItemDelegate):
#     def createEditor(self, parent, option, index):
#         line_edit = QLineEdit(parent)
#         return line_edit

#     def setEditorData(self, editor, index):
#         editor.setText(index.data())

#     def setModelData(self, editor, model, index):
#         model.setData(index, editor.text())



# class DelegateLWidget(QStyledItemDelegate):
#     def createEditor(self, parent, option, index):
#         list_widget = QListWidget(parent)
#         list_widget.setSelectionMode(QAbstractItemView.ExtendedSelection)
#         list_widget.addItems('One Two Three Four'.split()) # or whatever
#         return list_widget

#     def setEditorData(self, editor, index):
#         for line in index.data().splitlines():
#             for item in editor.findItems(line, Qt.MatchExactly):
#                 item.setSelected(True)

#     def setModelData(self, editor, model, index):
#         text = '\n'.join(item.text() for item in editor.selectedItems())
#         model.setData(index, text)

from PySide2.QtWidgets import QDialog, QVBoxLayout, QFormLayout, QDialogButtonBox, QLineEdit, QMessageBox,QDateEdit
from PySide2.QtGui import QIcon,QIntValidator,QDoubleValidator
from PySide2.QtCore import QDate




class EditDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Edit data")
        self.setWindowIcon(QIcon("resources/icons/pencil.png"))
        self.resize(400, 300)

        self.dialog_layout = QVBoxLayout()
        self.form_layout = QFormLayout()

        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Save | QDialogButtonBox.Cancel)

        # uvezivanje funkcija koje ce se pozivati kada se okine odredjeni dugmic
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        # populisanje layout-a
        self.dialog_layout.addLayout(self.form_layout)
        self.dialog_layout.addWidget(self.button_box)
        
        self.setLayout(self.dialog_layout)

    def enter_data(self, header_data, row):
        """
        Upisuje podatke u LineEdit widget-e na osnovu sadrzaja reda tabele koja je selektovana
        :param labels: nazivi kolona (labela) koje ce se prikazati
        :param data: vrednosti koje ce se upisati u input-e (LineEdit)
        :param read_only: nazivi kolona koje se ne mogu menjati kroz dijalog (primarni kljucevi)
        """
        self.inbound = self.parent().inTabs
        self.headerdata = header_data
        self.data = row
        self.tableSelections = []
        self.fields = []

        for link in self.headerdata.inbound_links:
            for intab in self.inbound:
                if link["file_path"] in intab["header"].abs_path:
                    self.tableSelections.append({"local":link["local_columns"],"foreign":link["foreign_columns"],"table":intab})
                    ...
            
            ...
        # metoda pravi redove u formi tako da su labele iz liste labela
        # a na odgovarajucim pozicijama ce se dobaviti i podaci iz liste data
        if len(self.data) == 0:
            # ako je lista podataka prazna, samo napraviti prazan sadrzaj
            for i in range(len(self.headerdata.column_descriptions)):
                if "varchar" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit()}
                    field["input"].setMaxLength(length)

                elif "char" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit(),"min":length}
                    field["input"].setMaxLength(length)

                elif "date" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit()}
                    self.fields.append(field)
                elif "int" in self.headerdata.column_descriptions[i]["type"]:
                    field = {"input":QLineEdit()}
                    field["input"].setValidator(QIntValidator())
                else:
                    field = {"input":QLineEdit()}
                self.fields.append(field)
                self.form_layout.addRow(self.headerdata.column_descriptions[i]["label"], field["input"])
        else:
            for i in range(len(self.headerdata.column_descriptions)):
                if "varchar" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit(str(self.data[i]))}
                    field["input"].setMaxLength(length)

                elif "char" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit(str(self.data[i])),"min":length}
                    field["input"].setMaxLength(length)

                elif "date" in self.headerdata.column_descriptions[i]["type"]:
                    length = int(self.headerdata.column_descriptions[i]["type"].split("(")[1].replace(")",""))
                    field = {"input":QLineEdit(str(self.data[i]))}
                    self.fields.append(field)
                elif "int" in self.headerdata.column_descriptions[i]["type"]:
                    field = {"input":QLineEdit(str(self.data[i]))}
                    field["input"].setValidator(QIntValidator())
                else:
                    field = {"input":QLineEdit(str(self.data[i]))}

                if self.headerdata.column_descriptions[i]["identifying"]:
                    field.setReadOnly(True)
                self.form_layout.addRow(self.headerdata.column_descriptions[i]["label"], field["input"])

    def get_data(self):
        # metoda koja iscitava podatke iz forme i vraca kao rezultat
        data = []
        for row_number in range(1, self.form_layout.rowCount()*2, 2):
            # prikupljanje teksta iz lineedit-a
            data.append(self.form_layout.itemAt(row_number).widget().text())
        return data
    
    def check_data(self):
        return True
    
    def accept(self):
        result = self.check_data(self.not_null_labels)
        if result:
            return super().accept()
        return
    
    def selection_from_table(self,table):
        ...