import sys
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit, QTabWidget, QGridLayout, QVBoxLayout, QTableView, QHBoxLayout
from .test_table_view import TestTableView
from gui.widgets.table_view.table_editor import EditDialog

class Tab(QWidget):
    def __init__(self,parent,header_data,table_data,inbound_tabs,outbound_tabs):
        super().__init__(parent)
        self.header = header_data
        self.data = table_data
        self.inTabsDetails = inbound_tabs
        self.outTabsDetails = outbound_tabs

        self.table = TestTableView(self,table_data,header_data,editable=True)
        # self.table.doubleClicked.connect(self.parent().open_edit_form)

        self.inTabs = []
        self.outTabs = []

        self.intabWidget = QTabWidget()
        self.intabWidget.setTabsClosable(False)

        self.outtabWidget = QTabWidget()
        self.outtabWidget.setTabsClosable(False)

        for element in self.inTabsDetails:
            self.inTabs.append({"title":element["header"].title,"header":element["header"],"table":TestTableView(self,element["data"],element["header"])})

        for element in self.outTabsDetails:
            self.outTabs.append({"title":element["header"].title,"header":element["header"],"table":TestTableView(self,element["data"],element["header"])})

        for table in self.inTabs:
            self.intabWidget.addTab(table["table"],table["title"])

        for table in self.outTabs:
            self.outtabWidget.addTab(table["table"],table["title"])
        
        self.connectedWidget = QWidget(self)
        self.cwLayout = QHBoxLayout()
        self.cwLayout.addWidget(self.intabWidget)
        self.cwLayout.addWidget(self.outtabWidget)
        self.connectedWidget.setLayout(self.cwLayout)

        self.tabLayout = QVBoxLayout(self)
        self.tabLayout.addWidget(self.table)
        self.tabLayout.addWidget(self.connectedWidget)

        self.setLayout(self.tabLayout)
        ...