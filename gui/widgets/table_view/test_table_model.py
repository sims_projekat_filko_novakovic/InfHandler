from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt
import math
from ..file_widget.file_classes.File import FileDescription


class TestTableModel(QAbstractTableModel):
    def __init__(self, parent=None, data=None,description:FileDescription = None ,block_size = 15):
        super().__init__(parent)
        
        if description != None:
            self.table_description = description
            self.header_data = []
            for c in description.column_descriptions:
                self.header_data.append(c["label"])
            ...
        else:
            self.header_data = ["Indeks", "Ime", "Prezime"]

        self.table_data = data
        
        self.block_size = block_size
        self.current_block = 0
        self.current_block_data = []
        i = self.current_block * self.block_size
        while i < (self.current_block + 1) * self.block_size and i< len(self.table_data):
            self.current_block_data.append(self.table_data[i])
            i+=1
      #  [["270", "Marko", "Markovic"],
        # ["271","Petar","Petrovic"]]
        self.changes = []
        self.current_change = -1

    def get_element(self, index: QModelIndex,):
        if index.isValid():
             if index.row() < len(self.current_block_data):
                element = self.current_block_data[index.row()][index.column()]
                if element:
                    return element
        return self.current_block_data
    
    def get_number_of_blocks(self):
        return math.ceil((1.0 * self.table_data) / self.block_size)

    def rowCount(self, parent=...):
        return len(self.table_data)
    
    def columnCount(self, parent=...):
        return len(self.header_data)
    
    def refresh_block_data(self):
        self.current_block_data = []
        i = self.current_block * self.block_size
        while i < (self.current_block + 1) * self.block_size:
            self.current_block_data.append(self.table_data[i])
            i+=1
        ...

    def update_row(self,index: QModelIndex,new_value):
        if index.isValid():
            old_value = self.current_block_data[index.row()]
            self.changes.append({
                "type": "row_change",
                "old_value": old_value,
                "new_value": new_value,
                "row": self.current_block * self.block_size + index.row()
            })
            self.table_data[self.current_block * self.block_size + index.row()] = new_value
            self.current_change += 1
            self.refresh_block_data()
        ...
    
    def delete_row(self,index: QModelIndex):
        if index.isValid():
            old_value = self.table_data.pop(self.current_block * self.block_size + index.row())
            self.changes.append({
                "type": "row_delete",
                "old_value": old_value,
                "row": self.current_block * self.block_size + index.row()
            })
            self.current_change += 1
            self.refresh_block_data()
    
    def create_row(self,index: QModelIndex,new_value):
        if index.isValid():
            self.table_data.insert(self.current_block * self.block_size + index.row(),new_value)
            self.changes.append({
                "type": "row_insert",
                "new_value": new_value,
                "row": self.current_block * self.block_size + index.row()
            })
            self.current_change += 1
            self.refresh_block_data()
        ...
    
    def next_block(self):
        self.change_block(self.current_block+1)

    def previous_block(self):
        self.change_block(self.current_block-1)


    def change_block(self,destination):
        if destination >-1 and destination < math.ceil((1.0*len(self.table_data))/self.block_size):
            old_block = self.current_block
            self.current_block = destination
            self.changes.append({
                "type": "block_jump",
                "new_block": self.current_block,
                "old_block": old_block
            })
            self.current_change+=1
            self.refresh_block_data()
        ...

    def undo(self):
        if self.current_change > -1:
            current_change_data = self.changes[self.current_change]
            if current_change_data["type"] == "row_change":
                self.table_data[current_change_data["row"]] = current_change_data["old_value"]
                self.current_change -= 1
                self.refresh_block_data()
                ...
            elif current_change_data["type"] == "row_delete":
                self.table_data.insert(current_change_data["row"],current_change_data["old_value"])
                self.current_change -= 1
                self.refresh_block_data()
                ...
            elif current_change_data["type"] == "row_insert":
                self.table_data.pop(current_change_data["row"])
                self.current_change -= 1
                self.refresh_block_data()
                ...
            elif current_change_data["type"] == "block_jump":
                self.current_block = current_change_data["old_block"]
                self.current_change -= 1
                self.refresh_block_data()
                ...
        ...
    
    def redo(self):
        if self.current_change < len(self.changes) - 1:
            change_data = self.changes[self.current_change + 1]
            if change_data["type"] == "row_change":
                self.table_data[change_data["row"]] = change_data["new_value"]
                self.current_change += 1
                self.refresh_block_data()
                ...
            elif change_data["type"] == "row_delete":
                self.table_data.pop(change_data["row"])
                self.current_change += 1
                self.refresh_block_data()
                ...
            elif change_data["type"] == "row_insert":
                self.table_data.insert(change_data["row"],change_data["new_value"])
                self.current_change += 1
                self.refresh_block_data()
                ...
            elif change_data["type"] == "block_jump":
                self.current_block = change_data["new_block"]
                self.current_change += 1
                self.refresh_block_data()
                ...
        ...
    
    def data(self, index, role= ...):
        element = self.get_element(index)
        if role == Qt.DisplayRole:
            return element
    
    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section]
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)

       
    def doubleClick(self):
        for idx in self.table_data.selectionModel().selectedIndexes():
            row_number = idx.row()
            column_number = idx.column()
        self.table_data.doubleClicked.connect(self.formaIzmenaPodatka)

    def formaIzmenaPodatka(self):
        ...
    
    
