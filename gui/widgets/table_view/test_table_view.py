from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView
from gui.widgets.table_view.test_table_model import TestTableModel
from .table_editor import EditDialog

class TestTableView(QWidget):
    def __init__(self, parent=None, data=None,description = None,editable = False):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()
        self.table_view = QTableView()
        self.headerData = description
        self.table_model = TestTableModel(data = self.generate_dummy_data(data),description=description)
        self.table_view.setModel(self.table_model)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

    #Nasumicni studenti radi testiranja funkcionalnosti i izgleda programa
    def generate_dummy_data(self, data=None):
        if data==None:
            data = [
                ["123", "Marko", "Markovic"],
                ["321", "Petar", "Petrovic"],
                ["456", "Janko", "Jankovic"],
                ["535", "Sava", "Savanovic"],
                ["875", "Jovan", "Jovanovic"],
                ["111", "Milorad", "Peric"],
                ["277", "Milica", "Milic"],
                ["986", "Jovica", "Jovic"],
                ["290", "Djuradj", "Djurdjevic"],
                ["123", "Marko", "Markovic"],
                ["321", "Petar", "Petrovic"],
                ["456", "Janko", "Jankovic"],
                ["535", "Sava", "Savanovic"],
                ["875", "Jovan", "Jovanovic"],
                ["111", "Milorad", "Peric"],
                ["277", "Milica", "Milic"],
                ["986", "Jovica", "Jovic"],
                ["290", "Djuradj", "Djurdjevic"]
            ]
        return data
    
    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dialoga podacima iz reda tabele

        edit_dialog.enter_data(self.headerData, selected_data)
        # prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1: # izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)
            