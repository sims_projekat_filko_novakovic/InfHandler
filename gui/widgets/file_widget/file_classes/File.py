from .FileResources import FileIRDescription
import unicodecsv
import csv

class FileDescription(FileIRDescription):
    def __init__(self, parent=None, title=None, type=None, rel_path=None,description = None, abs_path=None, column_descriptions=None ):
        if description:
            super().__init__(parent,description["title"],description["type"],description["title"],abs_path)
            columns = []
            for c in description["content"]:
                columns.append(c)
            self.column_descriptions = columns
            self.inbound_links = []
            self.outbound_links = []
            for c in description["relations"]["to"]:
                con = {}
                con["file_path"] = c["file"]
                con["local_columns"] = c["code"]
                con["foreign_columns"] = c["filecode"]
                self.outbound_links.append(con)
            for c in description["relations"]["from"]:
                con = {}
                con["file_path"] = c["file"]
                con["local_columns"] = c["code"]
                con["foreign_columns"] = c["filecode"]
                self.inbound_links.append(con)
        else:
            super().__init__(parent, title, type, rel_path, abs_path)
            columns = []
            for d in column_descriptions:
                columns.append(d)
            self.column_descriptions = columns
            self.inbound_links = []
            self.outbound_links = []
        
        
    # TODO: spojiti __init__ metode
    # def __init__(self,parent,description,abs_path):
    #     super().__init__(parent,description["title"].split(".")[0],description["type"],description["title"],abs_path) #mozda ukloniti ovu izmenu na title
    #     columns = []
    #     for c in description["content"]:
    #         columns.append(c)
    #         self.column_descriptions = columns
    #     self.inbound_links = []
    #     self.outbound_links = []
        ...
    
    def get_abs_path(self):
        return self.abs_path

    def set_parent(self,parent):
        self.parent = parent

    def add_column(self,column_description):
        self.column_descriptions.append(column_description)
    
    def add_inbound_connection(self,connection):
        self.inbound_links.append(connection)

    def add_outbound_connection(self,connection):
        self.outbound_links.append(connection)

    def get_column_descriptions(self):
        ...
    
    def get_data(self):
        data = []
        with open(self.abs_path,"r",encoding='utf-8') as dataFile:
            dataReader = csv.reader(dataFile,delimiter=";")
            for row in dataReader:
                data.append(row)
            dataFile.close()
        return data
        ...
    

    #TODO: ovo prebaciti u widget za prikaz tabela
    def determine_used_fk(self):
        with open(self.abs_path,"r") as dataFile:
            dataReader = unicodecsv.reader(dataFile,delimiter=";",encoding = 'utf-8')
            fk_codes = []
            fk_column_indeces = []
            used_keys = {}
            for inbound_connection in self.connections["Roditelj_veze"]:
                fk_codes.append(inbound_connection["code"])
            for code_set in fk_codes:
                index_set = []
                for code in code_set:
                    for index,column in enumerate(self.column_descriptions):
                        if column["code"] == code:
                            index_set.append(index)
                            break
                fk_column_indeces.append(index_set)
            for index,indexSet in enumerate(fk_column_indeces):
                value_list = []
                for row in dataReader:
                    value_set = []
                    for Index in indexSet:
                        value_set.append(row[Index])
                    value_list.append(value_set)
                used_keys[fk_codes[index]]=value_list
            return used_keys

    def get_all_inbound_links(self):
        return self.inbound_links
    
    def get_all_outbound_links(self):
        return self.outbound_links

    def get_used_foreign_keys(self,key_set):
        all = self.determine_used_fk()
        return all[key_set]
        ...

    def children(self):
        return []
