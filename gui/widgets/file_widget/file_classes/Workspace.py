from .File import FileDescription
from .Folder import FolderDescription
from .FileResources import FileIRDescription
import glob,json

def find_paths(startpath,visible_children_paths):

    is_last_directory = False
    meta_fajlovi = [f
        for f in glob.glob(startpath + "\\*.meta.json", recursive=False)
    ]
    meta_to_check = []

    for path in meta_fajlovi:
        # print(path)
        temp = path.split("\\")
        # print(temp)
        if len(temp) == 1:
            temp = path.split("/")
        if temp[len(temp)-1][:-10] in visible_children_paths:
            # print(temp[len(temp)-1][:-10])
            meta_to_check.append(path)

    
    valid_meta_paths = []
    paths_to_check = []
    meta_dictionaries = []
    for f in meta_to_check:
        meta_dictionaries.append(json.load(open(f)))
    for index,f in enumerate(meta_dictionaries):
        tip = f.get("type")
        if tip == "folder":
                paths_to_check.append([startpath+"\\"+f["title"],f.get("content")])
                valid_meta_paths.append(meta_to_check[index])
    if len(paths_to_check)==0:
        is_last_directory = True
        for index,f in enumerate(meta_dictionaries):
                tip = f.get("type")
                if tip == "file":
                    valid_meta_paths.append(meta_fajlovi[index])
    
    if not is_last_directory:
        for p in paths_to_check:
            valid_meta_paths = valid_meta_paths + find_paths(p[0],p[1])
    # print(valid_meta_paths) #<- prazno
    return valid_meta_paths

class WorkspaceDescription(FileIRDescription):
    def __init__(self, parent, title, type, abs_path, visible_children):
        super().__init__(parent, title, type, abs_path, abs_path)
        self.all_children = []
        self.direct_children = []
        self.visible_children = visible_children
        # print(visible_children)
        self.load_descriptions()

    def load_descriptions(self):
        # print("pathfinding_arguments") #<-ovo vidi
        # print(self.abs_path)
        # print(self.visible_children)
        all_paths = find_paths(self.abs_path,self.visible_children)
        # print(all_paths)
        # print("all_paths") <-ovo je prazno
        # print(all_paths)
        elements = []
        for path in all_paths:
            # print(path)
            description = json.load(open(path))
            element_path_components = path.split("\\")
            element_path = ""
            for i,component in enumerate(element_path_components):
                if i< len(element_path_components)-1:
                    element_path += component + "\\"
            element_path += description["title"]
            if description.get("type") == "folder":
                elements.append(FolderDescription(description=description,abs_path=element_path))
            elif  description.get("type") == "file":
                elements.append(FileDescription(description=description,abs_path=element_path))
        index = 0
        while index < len(elements) - 1:
            index2 = index + 1
            while index2 < len(elements):
                if elements[index].get_abs_path().split("\\") == elements[index2].get_abs_path().split("\\")[:-1]:
                    elements[index2].set_parent(elements[index])
                    elements[index].add_child(elements[index2])
                index2 += 1
            index += 1
        for element in elements:
            # print(element.get_abs_path())
            if element.get_parent() == None:
                element.set_parent(self)
                self.direct_children.append(element)
        for element in elements:
            print(element.get_abs_path())
            self.all_children.append(element)
        # print(len(self.all_children))
        #print(elements) #<- elements je prazno
                
    def children(self):
        
        return self.direct_children
    
    def get_inbound_linked(self,file:FileDescription):
        linked = []
        for element in self.all_children:
            if isinstance(element,FileDescription):
                for link in file.get_all_inbound_links():
                    if link["file_path"] in element.get_abs_path():
                        linked.append(element)
                ...
            ...
        ...
        return linked

    def get_outbound_linked(self,file:FileDescription):
        linked = []
        for element in self.all_children:
            if isinstance(element,FileDescription):
                for link in file.get_all_outbound_links():
                    if link["file_path"] in element.get_abs_path():
                        linked.append(element)
                ...
            ...
        ...
        return linked
        ...
        

    def add_child(self,child):
        self.children.append(child)
    
    def remove_child(self,child):
        self.children.remove(child)