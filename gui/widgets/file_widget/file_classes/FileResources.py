from abc import ABC

class FileIRDescription(ABC):
    def __init__(self,parent,title,type,rel_path,abs_path):
        self.parent = parent
        #naziv
        self.title = title
        #tip(Workspace/Folder/File)
        self.type = type
        #relativna putanja od parenta(u slucaju da je workspace, onda je ista kao absolutna putanja)
        self.rel_path = rel_path
        #absolutna putanja do resursa
        self.abs_path = abs_path
    
    def get_type(self):
        return self.type
    
    def get_parent(self):
        return self.parent