from .FileResources import FileIRDescription

class FolderDescription(FileIRDescription):
    def __init__(self, parent= None, title= None, type= None, rel_path= None, abs_path= None,description = None):
        if description != None:
            super().__init__(parent,description["title"],description["type"],description["title"],abs_path)
            self.all_children = []
            self.has_folders = False
        else :
            super().__init__(parent, title, type, rel_path, abs_path)
            self.all_children = []
            self.has_folders = False

    def set_parent(self,parent):
        self.parent = parent
    
    def children(self):
        return self.all_children
    
    def get_abs_path(self):
        return self.abs_path
    
    def add_child(self,child):
        self.all_children.append(child)
    
    def remove_child(self,child):
        self.all_children.remove(child)