from PySide2.QtWidgets import QWidget,QDockWidget, QTreeView, QFileSystemModel,QVBoxLayout
from PySide2.QtCore import QDir,QModelIndex
import PySide2.QtCore
from .workspace_model import WorkspaceModel
from .file_classes.Workspace import WorkspaceDescription
from .file_classes.File import FileDescription

#temp
class WorkspaceWidget(QWidget):
    def __init__(self,title="Structure",parent = None, Workspace_description:WorkspaceDescription = None,central_widget=None):
        super().__init__(parent)
        self.structure_widget = QTreeView()
        self.central_widget = central_widget
        self.structure_model = WorkspaceModel(parent=self,workspace=Workspace_description)
        # self.structure_model.setRootPath(QDir.currentPath())
        self.structure_widget.setModel(self.structure_model)

        self.structure_widget.doubleClicked.connect(self.workspace_on_double_click)

        self.ww_layout = QVBoxLayout(self)
        self.ww_layout.addWidget(self.structure_widget)


    
    #placeholder
    
    def workspace_on_double_click(self,index:QModelIndex = None):
        item = self.structure_model.get_element(index)
        if isinstance(item,FileDescription):
            print("parent")
            print(self.parent())
            print("parent parent")
            print(self.parent().parent())
            print("parent parent parent")
            print(self.parent().parent().parent())
            self.parent().parent().parent().parent().central_widget.tab1.create_tab(item,item.get_data(),self.structure_model.workspace.get_inbound_linked(item),self.structure_model.workspace.get_outbound_linked(item))
        #TODO: povezati sa centralnim widgetom radi kreiranja tabova
        ...
