from PySide2.QtWidgets import QDockWidget, QTreeView, QFileSystemModel,QTabWidget
from PySide2.QtCore import QDir

from .file_widget.workspace_widget import WorkspaceDescription,WorkspaceWidget
import json

class StructureDockWidget(QDockWidget):
    def __init__(self, title="Structure", parent=None):
        super().__init__(title, parent)
        #self.structure_widget = TreeView()
        self.structure_widget = QTabWidget(parent=self,tabsClosable=True)

        self.setWidget(self.structure_widget)

    def add_workspace(self,workspace_description_path = "..resources\\data\\TestWorkspace.meta.json"):
        meta_description = json.load(open(workspace_description_path))
        path_components = workspace_description_path.split("\\")
        path_components = path_components[:-1]
        abs_path = ""
        for component in path_components:
            abs_path += component
            abs_path+= "\\"
        # print(abs_path)
        abs_path += meta_description["title"]
        workspace_description = WorkspaceDescription(None,meta_description["title"],"workspace",abs_path,meta_description["content"])
        new_workspace = WorkspaceWidget(title="Workspace",parent = self, Workspace_description=workspace_description)
        self.structure_widget.addTab(new_workspace,meta_description["title"])
        self.setWidget(self.structure_widget)
        ...