from PySide2.QtWidgets import QToolBar, QAction, QMessageBox
from PySide2.QtGui import QIcon
from gui.widgets.help_dialog.help_dialog import HelpDialog
from gui.widgets.table_view.data_view import AppDemo, Tab


class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title,parent)
        self.actions_dict = {}

        self.default_actions()

        self.bind_actions()

    def default_actions(self):
        
        new_action = QAction(QIcon("resources/icons/folder-open-document.png"), "New")

        save_action = QAction(QIcon("resources/icons/disk-black.png"), "Save")
        
        delete_action = QAction(QIcon("resources/icons/cross.png"), "Delete")
        
        help_action = QAction(QIcon("resources/icons/question.png"), "Help")
        
        about_action = QAction(QIcon("resources/icons/information.png"),"About")

        createtab_action = QAction(QIcon("resources/icons/information.png"),"Create Tab")

        about_action.setStatusTip("About program")

        self.actions_dict["new"] = new_action
        self.actions_dict["delete"] = delete_action
        self.actions_dict["help"] = help_action
        self.actions_dict["about"] = about_action
        self.actions_dict["save"] = save_action
        self.actions_dict["createtab"] = createtab_action

        self.addAction(new_action)
        self.addAction(save_action)
        self.addAction(delete_action)
        self.addAction(help_action)
        self.addAction(about_action) 
        self.addAction(createtab_action)

    def bind_actions(self):
        self.actions_dict["about"].triggered.connect(self.on_about_action)
        self.actions_dict["help"].triggered.connect(self.on_help_action)
        self.actions_dict["createtab"].triggered.connect(self.on_createtab_action)

    def on_about_action(self):
         QMessageBox.information(self.parent(), "About program", "Prototip predmetnog projekta iz SiMS-a.", QMessageBox.Ok)

    def on_help_action(self):
        dialog = HelpDialog(self.parent())
        dialog.exec_()
        
    def on_createtab_action(self):
        self.app.create_tab()
        
    def createAppDemo(self, app:AppDemo):
        self.app = app

