from PySide2.QtWidgets import QDialog, QVBoxLayout, QLabel, QDialogButtonBox
from PySide2.QtGui import QIcon

class HelpDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Help")
        self.setWindowIcon(QIcon("resources/icons/question.png"))
        self.resize(200, 150)

        self.dialog_layout = QVBoxLayout()
        self.help_text = QLabel("Prozor za pomoc",self)
        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        #Accepted i rejected korisno dalje u projektu
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.dialog_layout.addWidget(self.help_text)
        self.dialog_layout.addWidget(self.button_box)


        self.setLayout(self.dialog_layout)