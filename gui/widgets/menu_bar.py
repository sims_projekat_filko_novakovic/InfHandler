from PySide2.QtWidgets import QMenuBar, QMenu,QAction,QFileDialog
from PySide2.QtGui import QIcon
from gui.widgets.structure_dock_widget import StructureDockWidget


class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        self._populate_menues()

    def _populate_menues(self):
        actions = self.parent().get_actions()
        self.help_menu.addAction(actions["help"])
        self.help_menu.addAction(actions["about"])

        self.open_workspace_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open workspace")
        self.open_workspace_action.triggered.connect(self.new_workspace)
        self.file_menu.addAction(self.open_workspace_action)

        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)
    

    def new_workspace(self):
        file_name = QFileDialog.getOpenFileName(self, "Open workspace description", "/resources/data", "meta JSON (*.meta.json)")
        # print(file_name)
        if file_name[0] == "":
            return
        file_path = file_name[0].replace("/","\\")
        # print(file_path)
        self.parent().workspace_widget.add_workspace(file_path)
        ...