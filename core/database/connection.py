from pymysql import connect, cursors

def open_connection(host="localhost", user="root", password="root", database="db"):
    connection = connect(host=host,
                         user=user,
                         password=password,
                         database=database,
                         charset="utf8mb4",
                         cursorclass=cursors.DictCursor)
    return connection

def execute_statement(connection, sql_statement, args=None):
    """
    Funkcija koja izvrsava upis ka bazi
    :param connection: konekcija koja je otvorena pre postavljanja upita
    :param sql_statement: SQL upit
    :param args: lista argumenata koji ce doci na promenljive u upitu
    :return: None (za sve osim SELECT upita) ili rezultat SELECT upita
    """
    result = None
    with connection:
        with connection.cursor() as cursor:
            # TODO: mozda dodati i nezavisnost od velicine slova
            if sql_statement.startswith("SELECT"):
                cursor.execute(sql_statement, args)
                # dobavljanje svih redova koji zadovoljavaju uslove
                result = cursor.fetchall()
            elif sql_statement.startswith("INSERT") or sql_statement.startswith("DELETE") \
                or sql_statement.startswith("CREATE") or sql_statement.startswith("UPDATE"):
                cursor.execute(sql_statement, args)
                connection.commit()
    # zatvaranje konekcije
    return result


if __name__ == "__main__":
    from statements import *
    # TODO: Primer INSERT upita
    # conn = open_connection()
    # stmt = form_insert_statement(table= "predmet",
    #                             columns=["idpredmet", "sifra", "naziv", "skraceni_naziv", "silabus"], 
    #                             values=[None, "OISSIMS", "Specifikacija i modelovanje softvera", "SIMS", ""],
    #                             primary_keys=["idpredmet"])
    # print(stmt)
    # execute_statement(conn, stmt)
    # TODO: Primer DELETE upita
    # conn = open_connection()
    # stmt = form_delete_statement("predmet", ["sifra"], ["OISSIMS"])
    # print(stmt)
    # execute_statement(conn, stmt)
    # TODO: Primere UPDATE upita
    # conn = open_connection()
    # stmt = form_update_statement("predmet", ["idpredmet", "silabus"], [3, "Novi silabus predmeta koji je azuriran"],
    #                              primary_keys=["idpredmet"])
    # print(stmt)
    # execute_statement(conn, stmt)
    # TODO: Primere SELECT upita
    conn = open_connection()
    stmt = form_select_statement("predmet", "*")
    print(stmt)
    result = execute_statement(conn, stmt)
    print(result)
    # TODO: dobavljanje svih tabela iz odredjene seme
    # conn = open_connection()
    # stmt = get_all_tables("sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)
    # TODO: dobavljanje svih naziva kolona jedne tabele iz seme
    # conn = open_connection()
    # stmt = get_all_columns("customer", "sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)
    # TODO: dobavljanje primarnih kljuceva iz tabele
    # conn = open_connection()
    # stmt = get_primary_keys("film", "sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)
    # TODO: dobavljanje not null kolona
    # conn = open_connection()
    # stmt = get_not_null_columns("actor", "sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)
    # TODO: dobavljanje auto-inkrement kolona
    # conn = open_connection()
    # stmt = get_autoincrement_columns("actor", "sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)
    # TODO: dobavljanje svih kolona sa tipovima podataka
    # conn = open_connection()
    # stmt = get_all_columns_with_types("actor", "sakila")
    # print(stmt)
    # result = execute_statement(conn, stmt)
    # print(result)